﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBlab3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            FillGrid();
        }

        private void buttonFill_Click(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void buttonUpate_Click(object sender, EventArgs e)
        {
            sqlDataAdapter1.Update(dataSetBooks1);
        }

        private void FillGrid()
        {
            sqlDataAdapter1.Fill(dataSetBooks1);
        }
    }
}
