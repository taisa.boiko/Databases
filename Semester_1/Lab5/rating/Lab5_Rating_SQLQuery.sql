create view lab5_query2_01 as
	select Reiting.Kod_student, Predmet_plan.K_predmet, sum(Reiting.Reiting) as rateingSum
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by Reiting.Kod_student, Predmet_plan.K_predmet
select * from lab5_query2_01

create view lab5_query2_02 as
	select kod_group, count(Kod_stud) as studentsNumber
		from dbo_student
		group by Kod_group
select * from lab5_query2_02

create view lab5_query2_03 as
	select kod_group, count(K_predmet) as predmetCount
		from Rozklad_pids
		inner join Predmet_plan on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
		group by kod_group
select * from lab5_query2_03

create view lab5_query2_04 as
	select kod_group, count(K_zapis) as classesCount
		from Rozklad_pids
		group by kod_group
select * from lab5_query2_04

create view lab5_query2_05 as
	select kod_group, avg(Reiting) as avgGroupMark
		from Rozklad_pids
		inner join Reiting on Rozklad_pids.K_zapis = Reiting.K_zapis
		group by kod_group
select * from lab5_query2_05

create view lab5_query2_06 as
	select K_predmet, avg(Reiting) as avgPredmetMark
		from Reiting
		inner join Rozklad_pids on Rozklad_pids.K_zapis = Reiting.K_zapis
		inner join Predmet_plan on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
		group by K_predmet
select * from lab5_query2_06

create view lab5_query2_07 as
	select kod_student, k_predmet, avg(Reiting) as currentRateing
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by kod_student, k_predmet
select * from lab5_query2_07

create view lab5_query2_08 as
	select k_predmet, min(Reiting) as minRateing
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by k_predmet
select * from lab5_query2_08

create view lab5_query2_09 as
	select k_predmet, max(Reiting) as maxRateing
		from Reiting
		inner join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by k_predmet
select * from lab5_query2_09

create view lab5_query2_10 as
	select k_predmet, Zdacha_type, count(K_zapis) as classesCount
		from Rozklad_pids
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		group by k_predmet, Zdacha_type
select * from lab5_query2_10

create view lab5_query2_11 as
	select Spetsialnost.K_spets, count(distinct dbo_groups.Kod_group) as groupsCount
		from dbo_groups
		inner join Rozklad_pids on dbo_groups.Kod_group = Rozklad_pids.Kod_group
		inner join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		inner join Navch_plan on Predmet_plan.K_navch_plan = Navch_plan.K_navch_plan
		inner join Spetsialnost on Navch_plan.K_spets = Spetsialnost.K_spets
		group by Spetsialnost.K_spets
select * from lab5_query2_11