create view lab5_query1_01 as
	select count(K_tovar) as goodsCount 
		from tovar
select * from lab5_query1_01

create view lab5_query1_02 as
	select count(K_sotrud) as loborCount 
		from sotrudnik
select * from lab5_query1_02

create view lab5_query1_03 as
	select count(K_postach) as providerCount 
		from postachalnik
select * from lab5_query1_03

create view lab5_query1_04 as
	select k_tovar, sum(kilkist) as amount
		from zakaz_tovar 
		inner join zakaz on zakaz_tovar.K_zakaz = zakaz.K_zakaz
		where month(date_rozm) = month(getdate())
		group by k_tovar
select * from lab5_query1_04

create view lab5_query1_05 as
	select sum(kilkist*price) as month_sum
		from zakaz_tovar 
		inner join zakaz on zakaz_tovar.K_zakaz = zakaz.K_zakaz
		inner join tovar on tovar.K_tovar = zakaz_tovar.k_tovar
		where month(date_rozm) = month(getdate())
select * from lab5_query1_05

create view lab5_query1_06 as
	select K_postav, sum(kilkist*price) - sum(kilkist*price)*Znigka/100 as salesMoney
		from zakaz_tovar 
		inner join zakaz on zakaz_tovar.K_zakaz = zakaz.K_zakaz
		inner join tovar on tovar.K_tovar = zakaz_tovar.k_tovar
		group by K_postav, Znigka
select * from lab5_query1_06

create view lab5_query1_07 as
	select k_postav, count(k_zakaz) as zakazAmount
		from zakaz_tovar
		inner join (select k_postav, k_tovar
						from tovar
						where nazva = 'Milk') as milk
				on milk.K_tovar = zakaz_tovar.k_tovar
		group by k_postav
select * from lab5_query1_07

create view lab5_query1_08 as
	select tovar.K_tovar, avg(price * kilkist) as avgSum
		from zakaz_tovar
		inner join tovar on tovar.K_tovar = zakaz_tovar.k_tovar
		group by tovar.K_tovar
select * from lab5_query1_08

create view lab5_query1_09 as
	select zakaz.K_klient, city, sum(Price*Kilkist) as cost
		from klient
		inner join zakaz on klient.K_klient = zakaz.K_klient
		inner join zakaz_tovar on zakaz.K_zakaz = zakaz_tovar.K_zakaz
		inner join tovar on zakaz_tovar.k_tovar = tovar.K_tovar
		where city = 'Zhytomyr'
		group by zakaz.K_klient, city
select * from lab5_query1_09

create view lab5_query1_10 as
	select postachalnik.K_postach, avg(price) as avgPrice
		from tovar
		inner join postachalnik on postachalnik.K_postach = tovar.K_postav
		group by postachalnik.K_postach
select * from lab5_query1_10