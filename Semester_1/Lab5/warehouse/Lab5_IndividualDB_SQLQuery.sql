--1
select Materials.id, month(arrivalDate) as arrivalMonth, sum(materialAmount) as generalMonthAmount
	from Materials
	inner join IncomeNotes on Materials.id = IncomeNotes.materialId
	group by Materials.id, month(arrivalDate)

--2
select Contracts.materialId, sum(wareAmount - incomeForToday.incomedAmount) as residue
	from Contracts
	inner join Materials on Contracts.materialId = Materials.id
	inner join (select materialId, sum(materialAmount) as incomedAmount
					from IncomeNotes
					where month(arrivalDate) = month(getdate())
					group by materialId) incomeForToday
				on incomeForToday.materialId = Materials.id
	group by Contracts.materialId
	order by Contracts.materialId

--3
select materialId, sum(materialAmount) as amount
	from IncomeNotes
	where month(arrivalDate) = month(getdate()) and year(arrivalDate) = year(getdate())
	group by materialId

--4
declare @timeSpanBegin as Datetime
set @timeSpanBegin = '01.04.2017'
declare @timeSpanEnd as Datetime
set @timeSpanEnd = '30.05.2017'

select materialId, sum(materialAmount) as amount
	from IncomeNotes
	where arrivalDate between @timeSpanBegin and @timeSpanEnd
	group by materialId