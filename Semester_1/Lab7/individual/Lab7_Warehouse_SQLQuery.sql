--procedures
alter proc CountIncomesInPeriod
	@result int output, @timeSpanBegin datetime, @timeSpanEnd datetime
	as
begin
	set @result = (select count(*)
						from IncomeNotes
						where arrivalDate between @timeSpanBegin and @timeSpanEnd)
end

declare @val int
exec CountIncomesInPeriod @val output, '01.04.2017', '30.05.2017'
select @val


create proc CountGeneralIncomeForCurrentMonth @res int output as
begin
	set @res = (select sum(amount)
					from (select materialId, sum(materialAmount) as amount
								from IncomeNotes
								where month(arrivalDate) = month(getdate()) and year(arrivalDate) = year(getdate())
								group by materialId
							)sub )
end

declare @val int
exec CountGeneralIncomeForCurrentMonth @val output
select @val

create proc CountCurrentMonthResidue @res int output as
begin
	set @res = (select sum(residue)
					from(select Contracts.materialId, sum(wareAmount - incomeForToday.incomedAmount) as residue
							from Contracts
							inner join Materials on Contracts.materialId = Materials.id
							inner join (select materialId, sum(materialAmount) as incomedAmount
											from IncomeNotes
											where month(arrivalDate) = month(getdate()) and year(arrivalDate) = year(getdate())
											group by materialId) incomeForToday
										on incomeForToday.materialId = Materials.id
							group by Contracts.materialId)sub)
end

declare @val int
exec CountCurrentMonthResidue @val output
select @val

--triggers
create trigger PreventBigDelete on Providers instead of delete as
begin
	declare insertCur cursor for
		select id from deleted
	declare @providerId as int
	open insertCur
	fetch next from insertCur into @providerId
	while @@FETCH_STATUS = 0
	begin
		if (select count(*)
				from Contracts
				where providerId = @providerId) > 5
		begin
			raiserror('Can not delete big provider', 16, 1)
			rollback
		end
		else
		delete from Providers
			where id = @providerId
		fetch next from insertCur into @providerId
	end

	close insertCur
	deallocate insertCur
end

delete from Providers
	where id = 1

create trigger CheckMaterialOverflow on IncomeNotes instead of insert as
begin
	declare insertCur cursor for
		select providerId, materialId, materialAmount from inserted
	declare @providerId as int
	declare @materialId as int
	declare @materialAmount as int
	open insertCur
	fetch next from insertCur into @providerId, @materialId, @materialAmount
	while @@FETCH_STATUS = 0
	begin
		if (select sum(wareAmount)
				from Contracts
				where providerId = @providerId and materialId = @materialId) < @materialAmount
		begin
			update Contracts
				set wareAmount = @materialAmount
				where providerId = @providerId and materialId = @materialId
		end

		fetch next from insertCur into @providerId, @materialId, @materialAmount
	end

	close insertCur
	deallocate insertCur
end

insert into IncomeNotes(arrivalDate, materialAmount, materialId, providerId)
	values(getdate(), 50, 10, 2)
select * from Contracts

alter trigger RedirectOutcomes on FactoryDepartments for delete as
begin
	declare insertCur cursor for
		select id from deleted
	declare @factoryId as int
	open insertCur
	fetch next from insertCur into @factoryId
	while @@FETCH_STATUS = 0
	begin
		if (select count(*)
				from OutcomeNotes
				where departmentId is null) > 0
		begin
			update OutcomeNotes
				set departmentId = (select top 1 percent id from FactoryDepartments order by newid())
				where departmentId is null
		end
		fetch next from insertCur into @factoryId
	end

	close insertCur
	deallocate insertCur
end

select * from OutcomeNotes
delete from FactoryDepartments
	where id = 5
select * from OutcomeNotes