﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Lab5_Entity;

namespace Lab5_Entity.Controllers
{
    public class DistributedBooksController : Controller
    {
        private LibraryEntities db = new LibraryEntities();

        // GET: DistributedBooks
        public ActionResult Index()
        {
            var distributedBooks = db.DistributedBooks.Include(d => d.Books).Include(d => d.Readers);
            return View(distributedBooks.ToList());
        }

        // GET: DistributedBooks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributedBooks distributedBooks = db.DistributedBooks.Find(id);
            if (distributedBooks == null)
            {
                return HttpNotFound();
            }
            return View(distributedBooks);
        }

        // GET: DistributedBooks/Create
        public ActionResult Create()
        {
            ViewBag.bookId = new SelectList(db.Books, "id", "title");
            ViewBag.readerId = new SelectList(db.Readers, "id", "name");
            return View();
        }

        // POST: DistributedBooks/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,readerId,bookId,distributionDate,realReturDate,plannedReturDate")] DistributedBooks distributedBooks)
        {
            if (ModelState.IsValid)
            {
                db.DistributedBooks.Add(distributedBooks);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.bookId = new SelectList(db.Books, "id", "title", distributedBooks.bookId);
            ViewBag.readerId = new SelectList(db.Readers, "id", "name", distributedBooks.readerId);
            return View(distributedBooks);
        }

        // GET: DistributedBooks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributedBooks distributedBooks = db.DistributedBooks.Find(id);
            if (distributedBooks == null)
            {
                return HttpNotFound();
            }
            ViewBag.bookId = new SelectList(db.Books, "id", "title", distributedBooks.bookId);
            ViewBag.readerId = new SelectList(db.Readers, "id", "name", distributedBooks.readerId);
            return View(distributedBooks);
        }

        // POST: DistributedBooks/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,readerId,bookId,distributionDate,realReturDate,plannedReturDate")] DistributedBooks distributedBooks)
        {
            if (ModelState.IsValid)
            {
                db.Entry(distributedBooks).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.bookId = new SelectList(db.Books, "id", "title", distributedBooks.bookId);
            ViewBag.readerId = new SelectList(db.Readers, "id", "name", distributedBooks.readerId);
            return View(distributedBooks);
        }

        // GET: DistributedBooks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DistributedBooks distributedBooks = db.DistributedBooks.Find(id);
            if (distributedBooks == null)
            {
                return HttpNotFound();
            }
            return View(distributedBooks);
        }

        // POST: DistributedBooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DistributedBooks distributedBooks = db.DistributedBooks.Find(id);
            db.DistributedBooks.Remove(distributedBooks);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
