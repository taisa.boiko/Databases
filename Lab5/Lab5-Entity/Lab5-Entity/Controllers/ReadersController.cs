﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Lab5_Entity;

namespace Lab5_Entity.Controllers
{
    public class ReadersController : Controller
    {
        private LibraryEntities db = new LibraryEntities();

        // GET: Readers
        public ActionResult Index()
        {
            return View(db.Readers.ToList());
        }

        // GET: Readers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Readers readers = db.Readers.Find(id);
            if (readers == null)
            {
                return HttpNotFound();
            }
            return View(readers);
        }

        // GET: Readers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Readers/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,name,homeAddress,phoneNumber,isDroppedOut")] Readers readers)
        {
            if (ModelState.IsValid)
            {
                db.Readers.Add(readers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(readers);
        }

        // GET: Readers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Readers readers = db.Readers.Find(id);
            if (readers == null)
            {
                return HttpNotFound();
            }
            return View(readers);
        }

        // POST: Readers/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,homeAddress,phoneNumber,isDroppedOut")] Readers readers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(readers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(readers);
        }

        // GET: Readers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Readers readers = db.Readers.Find(id);
            if (readers == null)
            {
                return HttpNotFound();
            }
            return View(readers);
        }

        // POST: Readers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Readers readers = db.Readers.Find(id);
            db.Readers.Remove(readers);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
