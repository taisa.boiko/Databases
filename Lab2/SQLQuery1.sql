use Library
create user library_user for login test_login

create role reader authorization library_user
create role librarian authorization library_user
create role manager authorization library_user

alter role reader add member library_user
alter role librarian add member library_user
alter role manager add member library_user
exec sp_helprolemember

grant select on books to reader
grant select, update, insert on books to librarian
grant select, update, insert on distributedBooks to librarian

grant all on readers to librarian

