﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CokingPlace.Startup))]
namespace CokingPlace
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
