﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace CokingPlace.Models
{
    public class Recipe
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public string Ingredients { get; set; }
        [NotMapped]
        public List<string> IngridientsList => Ingredients.Split(new char[] { '\n', '\\' }).
                        Where(x => !string.IsNullOrEmpty(x)).ToList();
        public int CategoryId { get; set; }
        public string UserId { get; set; }

        [Required]
        public int CookingTime { get; set; }

        [Required]
        public string CookingProcess { get; set; }
        public string ImageUrl { get; set; }
        public DateTime CreationDate { get; set; }

        /*[NotMapped]
        public bool IsLikedByCurrentUser {
            get
            {
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                try
                {
                    var currentUser = manager.FindById(UserId);
                    return currentUser.LikedResipes.Exists(x => x.Id == Id);
                }
                catch
                {
                    return false;
                }
            }
            set
            {
                var context = new ApplicationDbContext();
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
                var currentUser = manager.FindById(UserId);

                if (value)
                {
                    if (currentUser.LikedResipes == null)
                        currentUser.LikedResipes = new List<Recipe> { this };
                    else currentUser.LikedResipes.Add(this);
                }
                else
                {
                    currentUser.LikedResipes.Remove(this);
                }

                context.SaveChanges();
            }
        }*/
        public string UserName { get; set; }
    }
}