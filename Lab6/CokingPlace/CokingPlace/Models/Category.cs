﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CokingPlace.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CoverImgUrl { get; set; }
        public List<Recipe> Recipes { get; set; }
    }
}