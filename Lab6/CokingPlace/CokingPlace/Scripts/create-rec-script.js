﻿function reasUrl(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.image-add img, .image-change img').attr('src', e.target.result).width("100%");
            $('.image-add').css({ 'background': "transparent", "padding": "0" });
            $('.image-add span').text("Змініть зображення");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$('#image-file').change(function () {
    reasUrl(this);
})


$('#add-ingridient').click(function () {
    if ($('.ingridients-list li').size() > 15) {
        alert("Ви не можете додавати більше 15 інгредієнтів");
        return;
    }

    var ingName = $('#ingredient-name').val(), ingAmount = $('#amount').val();
    if (ingName.length && ingAmount.length) {
        $('<li />', { html: ingName + ' ' + ingAmount }).appendTo('ul.ingridients-list');
        /*$('<input>').attr({
            type: 'hidden',
            name: 'Ingredients',
            value: ingName + ' ' + ingAmount
        }).appendTo('form');*/

        $('#ingredient-name').val("");
        $('#amount').val("");
    }
});

$('#del-ingridient').click(function () {
    $('.ingridients-list li:last-child').remove();
});

$('input[type=submit]').click(function () {
    var input = $('input[name=Ingredients]');
    input.val("");
    $('.ingridients-list li').each(function () {
        input.val(input.val() + $(this).text() + '\\');
    });
});

$('input[type=reset]').click(function () {
    $('ul.ingridients-list').html('');
});
