﻿using CokingPlace.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CokingPlace.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            CookingBookDbContext db = new CookingBookDbContext();
            try
            {
                ViewBag.LastRecipes = db.Recipes.OrderBy(i => i.CreationDate).Take(9).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}